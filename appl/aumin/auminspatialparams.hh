// $Id$
/*****************************************************************************
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_AUMIN_SPATIAL_PARAMS_HH
#define DUMUX_AUMIN_SPATIAL_PARAMS_HH

//#include < dumux/meterial/spatialparams/implicit.hh>
#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh> // capillary pressure model is Van Genuchten.
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>


namespace Dumux
{
//forward declaration
template<class TypeTag>
class AuMinSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(AuMinSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(AuMinSpatialParams, SpatialParams, Dumux::AuMinSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(AuMinSpatialParams, MaterialLaw) // EffMaterialLaw
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
}

/**
 * \brief Definition of the spatial parameters for the brine-co2 problem
 *
 */
template<class TypeTag>
class AuMinSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename Grid::ctype CoordScalar;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
    };

//    typedef typename GET_PROP_TYPE(TypeTag, TwoPNCMinIndices)) Indices;
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> Vector;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    AuMinSpatialParams(const GridView &gv)
        : ParentType(gv)
    {


        try
        {
        porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
        critPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, CritPorosity);
        SandK_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
        //SandKhet_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability2);
       //SandKhet2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability3);
        }
        catch (Dumux::ParameterException &e)
        {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        for (int i = 0; i < dim; i++)
            K_[i][i] = SandK_;

        // residual saturations
                columnMaterialParams_.setSwr(0.2);
                columnMaterialParams_.setSnr(0.3);

        // parameters for vG law
           //     columnMaterialParams_.setVgAlpha(2.847e-5); // TODO: this needs to checked
                columnMaterialParams_.setVgAlpha(3.649337e-4);
                columnMaterialParams_.setVgn(3.1365);
    }

    ~AuMinSpatialParams()
    {}


    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution The global solution vector
     */
    void update(const SolutionVector &globalSolution)
    {
    };

//     /*!
//      * \brief Apply the intrinsic permeability tensor to a pressure
//      *        potential gradient.
//      *
//      * \param element The current finite element
//      * \param fvElemGeom The current finite volume geometry of the element
//      * \param scvfIdx The index sub-control volume face where the
//      *                      intrinsic velocity ought to be calculated.
//      */
//     const Scalar intrinsicPermeability(const Element &element,
//                                        const FVElementGeometry &fvElemGeom,
//                                        int scvIdx) const
//     {
//         const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
// ///////
// // Heterogeneity/ Horizontal column with different permeability.
// //////
// //    if ((0.1 <= globalPos[1])&& (globalPos[1]<=0.15)||(0.25<=globalPos[1])&& (globalPos[1]<= 0.3)||(0.4<=globalPos[1])&& (globalPos[1]<= 0.43))
// //      return  SandKhet_;
// //    else
//      // return SandK_;
// ///////
// // Heterogeneity/ Vertical  column with different permeability.
// //////
// //      if ((0.1 < globalPos[0])&& (globalPos[0]<0.2)||(0.3 < globalPos[0])&& (globalPos[0]<0.4)||(0.5<globalPos[0])&& (globalPos[0]< 0.6))
// //               return  SandKhet_;
// //         else
// //               return SandK_;
// ////////
// // Heterogeneity/ slope  column with different permeability.
// ///////
// //     Scalar x= globalPos[0];
// //     Scalar y = globalPos[1];
// //     if (globalPos[1]<= std::sin(x)&& globalPos[1]>= x*std::sin(x))
// //           return  SandKhet_;
// //            else if
// //                ( globalPos[1] > std::cos(x))
// //            return SandK_;
// //            else
// //            return SandKhet2_;
//         return SandK_;
//
//     }
        /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param fvGeometry The finite-volume geometry in the box scheme
     *  \param scvIdx The local vertex index
     *
     *  Alternatively, the function intrinsicPermeabilityAtPos(const GlobalPosition& globalPos)
     *  could be defined, where globalPos is the vector including the global coordinates
     *  of the finite volume.
     */
    const Tensor intrinsicPermeability(const Element &element,
                                        const FVElementGeometry &fvGeometry,
                                        const int scvIdx) const
    {
        return K_;
    }

        /*!
     * \brief Define the minimum porosity \f$[-]\f$ after salt precipitation
     *
     * \param elemVolVars The data defined on the sub-control volume
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    double porosityMin(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
     {
        return 0.0;
     }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param vDat The data defined on the sub-control volume
     * \param element The finite element
     * \param fvElemGeom The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    double solidity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
            return 1 - porosity_;

    }
    const Scalar porosity(const Element &element,               //TODO sonst gibt es einen Fehler: Dune reported error: Dune::InvalidStateException [porosityAtPos:/temp/hommel/DUMUX/dumux/dumux/material/spatialparams/boxspatialparams1p.hh:167]: The spatial parameters do not provide a porosityAtPos() method.
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
        return porosity_;
    }
    const Scalar critPorosity(const Element &element,
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
        return critPorosity_;
    }
//    Scalar porosityAtPos(const GlobalPosition& globalPos) const
//    {
//        DUNE_THROW(Dune::InvalidStateException,
//                   "The spatial parameters do not provide "
//                   "a porosityAtPos() method.");
//    }
    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
            return columnMaterialParams_;
    }

  struct currentOilResidualSaturations
    {
      Scalar curSor;
    };

    // the oil residual saturation is updated dependent on a number of
    // parameters; this is done in 2pbiobergenvolumevariables.hh
    void currentSor(Problem &problem)
    {
        // get the number of degrees of freedom
        FVElementGeometry fvGeometry;
        VolumeVariables volVars;
        curSor_.resize(problem.model().numDofs());

        for (const auto& element : elements(problem.gridView()))
        {
            fvGeometry.update(problem.gridView(), element);
            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
            {
                const unsigned int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);
                volVars.update(problem.model().curSol()[dofIdxGlobal],
                               problem,
                               element,
                               fvGeometry,
                               scvIdx,
                               false);
                curSor_[dofIdxGlobal].curSor = volVars.sorReturn();

                columnMaterialParams_.setSnr(curSor_[dofIdxGlobal].curSor);
                // TODO: the line above is actually the required one
                //       the line below de facto gives assigns a constant value to Snr, in this 0.05
                //       which means that all the calculations would have been just for one.
                // Thus, once all parameters are correct, one should use the line above.
                // columnMaterialParams_.setSnr(0.30);
            }
        }
    }


private:

    Tensor K_;
    Scalar SandK_;
   // Scalar SandKhet_;
   // Scalar SandKhet2_;
    Scalar porosity_;
    Scalar critPorosity_;
    std::vector<currentOilResidualSaturations> curSor_;
    MaterialLawParams columnMaterialParams_;
};

}

#endif
