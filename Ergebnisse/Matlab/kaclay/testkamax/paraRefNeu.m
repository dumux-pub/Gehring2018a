% ========================================================================
% Arbitraty Integrative Probabilistic Collocation Method
% Author: Dr. Sergey Oladyshkin
% SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 61, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% ========================================================================
%modified by Lena Walter
clear all;

%-------------------------------------------------------------------------------------------
%---------- Reading VTU's
%-------------------------------------------------------------------------------------------


%         %--->Reading of Names for Simulation Output Files
%         fid = fopen('AuMin_including_kaClayOM_redSubstrate_newCNYields_noSubBC_largerGrid_noInitAu_moreInjSub_increasedPrec.pvd', 'r');
%         fid = fopen('AuMin_including_kaClayOM_kprecClayOM.pvd', 'r');
fid = fopen('AuMin.pvd', 'r');
        i=0;
        while feof(fid) == 0
            line = fgetl(fid);
            if (findstr(line,'DataSet')~=0)
                i=i+1;
                ref_Time(i)=str2num(line(findstr(line,'timestep="')+length('timestep="'):findstr(line,'" f')-1));
                ref_SimulationOutputFileNames{i}=line(findstr(line,'file="')+length('file="'):findstr(line,'"/>')-1);
            end
        end
        fclose(fid);

                %--->Reading Number of Points/Cells and Box/Cell-Volumes
        fid = fopen(strcat(ref_SimulationOutputFileNames{1}), 'rt');
        while feof(fid) == 0
        line = fgetl(fid);
                if (findstr(line,'NumberOfPoints=')~=0)
                    ref_NumberOfPoints=str2num(line(findstr(line,'NumberOfPoints="')+length('NumberOfPoints="'):findstr(line,'">')-1));
                end
                if (findstr(line,'Name="boxVolume"')~=0)
                    ref_vol=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'NumberOfCells=')~=0)
                    ref_NumberOfCells=str2num(line(findstr(line,'NumberOfCells="')+length('NumberOfCells="'):findstr(line,'">')-1));
                end
                if (findstr(line,'Name="cell')~=0) & (~isempty(strfind(line,'volume')~=0))
                    ref_cellVolume=fscanf(fid, '%g', [1 ref_NumberOfCells]);
                end

        end
        fclose(fid);

        %---> Reading from all Output Simulation Files
        for j=1:1:length(ref_SimulationOutputFileNames)
            fid = fopen(strcat(ref_SimulationOutputFileNames{j}), 'rt');
            i=0;
            while feof(fid) == 0
                i=i+1;
                line = fgetl(fid);
                if (findstr(line,'Name="mobG"')~=0)
                   ref_mobG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="mobL"')~=0)
                   ref_mobL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="pc"')~=0)
                   ref_pc(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="pl"')~=0)
                   ref_pl(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="pg"')~=0)
                   ref_pg(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="porosity"')~=0)
                   ref_porosity(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="temperature"')~=0)
                   ref_temperature(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="Sl"')~=0)
                   ref_Sl(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="Sg"')~=0)
                   ref_Sg(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="rhoG"')~=0)
                    ref_rhoG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="rhoL"')~=0)
                    ref_rhoL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end


                if (findstr(line,'Name="precipitateVolumeFraction_au"')~=0)
                   ref_volFracAu(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="precipitateVolumeFraction_d"')~=0)
                   ref_volFracDissBac(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end

                if (findstr(line,'Name="precipitateVolumeFraction_p"')~=0)
                   ref_volFracPrecBac(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end

                if (findstr(line,'Name="precipitateVolumeFraction_nanoAu"')~=0)
                   ref_volFracNanoAu(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end


                if (findstr(line,'Name="Permeability"')~=0)
                   ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="Kxx"')~=0)
                   ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end


                if (findstr(line,'Name="Coordinates"')~=0)
                   ref_Coordinates(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints*3]);
                end
                if (findstr(line,'Name="m^w_cyanide"')~=0)
                   ref_MolarityCN(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="m^w_citrate"')~=0)
                   ref_MolarityCitrate(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="m^w_substrate"')~=0)
                   ref_MolaritySubstrate(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="m^w_AuComplex"')~=0)
                   ref_MolarityAuComplex(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end
                if (findstr(line,'Name="m^w_nanoAu"')~=0)
                   ref_MolarityNanoAu(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
                end

            end
            fclose(fid);

        end

save('AuMin.mat');


