clear;
load('std2/AuMin.mat');
%plot over line
timeFilePara = 38 %length(ref_Time);% hier den Zeitschritt wählen

for i=1:4

    Standard(i)      = ref_MolarityAuComplex(timeFilePara, 24+i);
    depth(i) = 1/60 + (6-i)/30; 
end


load('aAuNP/aAuNP95/AuMin.mat');
for i=1:4

    aAuNP95(i)      = ref_MolarityAuComplex(timeFilePara,  24+i);
end


load('aAuNP/aAuNP105/AuMin.mat');
for i=1:4

    aAuNP105(i)      = ref_MolarityAuComplex(timeFilePara,  24+i);
end

load('aAuNP/aAuNPmin/AuMin.mat');
for i=1:4

    aAuNPmin(i)      = ref_MolarityAuComplex(timeFilePara,  24+i);
end

load('aAuNP/aAuNPmax/AuMin.mat');
for i=1:4

    aAuNPmax(i)      = ref_MolarityAuComplex(timeFilePara,  24+i);
end


Fig = figure;
figure(Fig);

h1_plot = plot(depth, Standard, 'k-', depth, aAuNP95, 'b-', depth, aAuNP105, 'r-', depth, aAuNPmin, 'b--', depth, aAuNPmax, 'r--');
h1_leg = legend('Standard','aAuNP95','aAuNP105','aAuNPMin','aAuNPMax','location','northeast');
h1_xlab = xlabel('Depth [m]');
h1_ylab = ylabel('Concentration [mol/m³]');
% axis([0.05 0.025  2.69e-4 2.7e-4]);


set(gca,'FontSize',16)
% set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
% set(h1_plot,'MarkerSize',8);


% set(Fig,'position',[0 0, 800 550])
set(Fig,'position',[0 0, 800 600])
set(Fig,'PaperPositionMode','auto')
saveas(Fig,'/home/hommel/matlab/new_graph','epsc') 



