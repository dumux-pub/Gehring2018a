clear;
load('std2/AuMin.mat');
%plot over line
timeFilePara = length(ref_Time);% hier den Zeitschritt wählen

for i=1:21

    Standard(i)      = ref_MolarityNanoAu(timeFilePara, 1+i);
    depth(i) = 1/60 + (29-i)/30; 
end


load('Ks/Ks95/AuMin.mat');
for i=1:21

    Ks95(i)      = ref_MolarityNanoAu(timeFilePara,  1+i);
end

load('Ks/Ks105/AuMin.mat');
for i=1:21
    Ks105(i)      = ref_MolarityNanoAu(timeFilePara,  1+i);
end

load('Ks/Ksmin/AuMin.mat');
for i=1:21

    Ksmin(i)      = ref_MolarityNanoAu(timeFilePara,  1+i);
end

load('Ks/Ksmax/AuMin.mat');
for i=1:21

    Ksmax(i)      = ref_MolarityNanoAu(timeFilePara,  1+i);
end


Fig = figure;
figure(Fig);

h1_plot = plot(depth, Standard, 'k-', depth, Ks95, 'b-', depth, Ks105, 'r-', depth, Ksmin, 'b--', depth, Ksmax, 'r--');
h1_leg = legend('Standard','Ks95','Ks105','KsMin','KsMax','location','northeast');
h1_xlab = xlabel('Depth [m]');
h1_ylab = ylabel('Concentration [mol/m³]');
% axis([0.05 0.025  2.69e-4 2.7e-4]);


set(gca,'FontSize',16)
% set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
% set(h1_plot,'MarkerSize',8);


% set(Fig,'position',[0 0, 800 550])
set(Fig,'position',[0 0, 800 600])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'/home/hommel/matlab/new_graph','epsc') 



