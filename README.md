Summary
=======

DUMUX pub module for the Bachelors Thesis of Luca Gehring

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installGehring2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Gehring2018a/raw/master/installGehring2018a.sh)
in this folder.

```bash
mkdir -p Gehring2018a && cd Gehring2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Gehring2018a/raw/master/installGehring2018a.sh
sh ./installGehring2018a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/aumin/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/aumin/aumin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.



Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.

- DUNE 2.5 (common, grid, istl, geometry, localfunctions)
- dumux-stable 2.11


Results
=======

The final .vtus and .mats for the simulated scenarios can be found in the /Ergebnisse folder.

All results discussed in Chapter 4 of the thesis are assuming zero background concentrations of Au species.


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Gehring2018a
cd Gehring2018a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Gehring2018a/-/raw/master/docker_gehring2018a.sh
```

Open the Docker Container
```bash
bash docker_gehring2018a.sh open
```

After the script has run successfully, you may build the executable by running

```bash
cd Gehring2018a/build-cmake
make build_tests
```

It is located in the build-cmake/appl/aumin folder. It can be executed with an input file e.g., by running

```bash
cd appl/aumin
./aumin aumin.input
```
