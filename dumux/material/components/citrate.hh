/*
 * Na.hh
 *
 *  Created on: 28.06.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the Cl- fluid properties
 */
#ifndef DUMUX_CITRATE_HH
#define DUMUX_CITRATE_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Na (Natrium ion) fluid properties
 */
template <class Scalar>
class Citrate : public Component<Scalar, Citrate <Scalar> >
{
public:
    /*!
     * \brief A human readable name for the cianide.
     */
    static const char *name()
    { return "Citrate"; }

    /*!
     * \brief The mass in [kg] of one mole of CN.
     */
    static Scalar molarMass()
    { return 189.099e-3; } //kg/mole

    /*!
     * \brief The diffusion Coefficient of Cl in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 6.23e-10; } // 2.28e-5 cm²/s or 2.28e-9 m²/s

    static Scalar charge()
    {
        return -1.0;
    }

};

} // end namespace

#endif