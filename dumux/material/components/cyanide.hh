/*
 * Na.hh
 *
 *  Created on: 28.06.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the Cl- fluid properties
 */
#ifndef DUMUX_CYANIDE_HH
#define DUMUX_CYANIDE_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Na (Natrium ion) fluid properties
 */
template <class Scalar>
class Cyanide : public Component<Scalar, Cyanide <Scalar> >
{
public:
    /*!
     * \brief A human readable name for the cianide.
     */
    static const char *name()
    { return "Cyanide"; }

    /*!
     * \brief The mass in [kg] of one mole of CN.
     */
    static Scalar molarMass()
    { return 26.02e-3; } //kg/mole

    /*!
     * \brief The diffusion Coefficient of Cl in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2.28e-9; } // 2.28e-5 cm²/s or 2.28e-9 m²/s

    static Scalar charge()
    {
        return -1.0;
    }

};

} // end namespace

#endif