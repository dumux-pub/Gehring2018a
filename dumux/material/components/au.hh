// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \ingroup Components
 * \brief Abstract base class of a pure chemical species.
 */
#ifndef DUMUX_AU_HH
#define DUMUX_AU_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

//#include <dune/common/stdstreams.hh>

//#include <dumux/implicit/2pbiomin/properties.hh>

#include <cmath>
#include <iostream>

namespace Dumux
{

/*!
 * \ingroup Components
 * \brief Abstract base class of a pure chemical species.
 *
 * \tparam Scalar The type used for scalar values
 * \tparam Implementation Necessary for static polymorphism
 */

template <class Scalar>
class Au :public Component<Scalar, Au<Scalar> >
{
public:

  /*!
     * \brief A human readable name for Au(Gold)
     */
   static const char *name()
    { return "Au"; }
    //{ DUNE_THROW(Dune::NotImplemented, "Component::name()"); }


    static Scalar density()
    {
      return 19300; /* 19300 kg/m³*/
    }


    /*!
     * \brief The mass in [kg] of one mole of Gold.
     */
    static Scalar molarMass()
    {return 196.97e-3 ; }// kg/mol

};

} // end namespace

#endif