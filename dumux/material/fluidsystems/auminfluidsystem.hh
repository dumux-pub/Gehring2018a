/*****************************************************************************
 *   Copyright (C) 2012 by Johannes Hommel
 *
 *   Copyright (C) 2008-2010 by Melanie Darcis                               *
 *   Copyright (C) 2009-2010 by Andreas Lauser                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A fluid system with brine and air as phases and brine and
 *        as components./// Phase , Componenets !? in aumin
 */
#ifndef DUMUX_AUMINFLUID_SYSTEM_HH
#define DUMUX_AUMINFLUID_SYSTEM_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/fluidsystems/base.hh>



#include <dumux/material/components/brine.hh>
#include <dumux/material/components/water.hh>
#include <dumux/material/components/biosub.hh>
#include <dumux/material/components/au.hh>
// #include <dumux/material/components/solidau.hh>
// #include <dumux/material/components/mobilenanoau.hh>
#include <dumux/material/components/cyanide.hh>
#include <dumux/material/components/citrate.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/components/biofilm.hh>

//#include <dumux/material/components/xylene.hh>  // for oil phase
//#include <dumux/material/components/biosusp.hh>
//#include <dumux/material/components/bioproduct.hh>
//#include <dumux/material/binarycoefficients/h2o_xylene.hh> // for diffusion we need it (all the file in binarycoefficients)
#include <dumux/material/binarycoefficients/h2o_air.hh>

#include <dumux/material/fluidsystems/nullparametercache.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/exceptions.hh>

#include <assert.h>

#ifdef DUMUX_PROPERTIES_HH
#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>
#endif

namespace Dumux
{
// #include <dumux/material/components/co2tables.inc>
namespace FluidSystems
{
/*!
 * \brief TODO: New explanation!!!!!!!!!!!!!!!!!  A compositional fluid with brine and carbon as
 *        components in both, the liquid and the gas (supercritical) phase,
 *        additional biomineralisation components and solid phases.
 *
 * This class provides acess to the Bio fluid system when no property system is used.
 * For Dumux users, using BioFluid<TypeTag> and the documentation therein is
 * recommended.
 *
 *  The user can provide their own material table for co2 properties.
 *  This fluidsystem is initialized as default with the tabulated version of
 *  water of the IAPWS-formulation, and the tabularized adapter to transfer
 *  this into brine.
 *  In the non-TypeTagged version, salinity information has to be provided with
 *  the init() methods.
 */
template <class TypeTag,
    class Scalar,
    class H2O_Tabulated = Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> >
class AuMinFluid: public BaseFluidSystem <Scalar, AuMinFluid <TypeTag, Scalar, H2O_Tabulated> >
{

    typedef AuMinFluid<TypeTag, Scalar, H2O_Tabulated> ThisType;
    typedef BaseFluidSystem <Scalar, ThisType> Base;
//     typedef Dumux::IdealGas<Scalar> IdealGas;

public:
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O;
    typedef Dumux::Au<Scalar> Au;
//     typedef Dumux::SolidAu<Scalar> SolidAu;
//     typedef Dumux::MobileNanoAu<Scalar> MobileNanoAu;
    typedef Dumux::Brine<Scalar, H2O> Brine;
    typedef Dumux::Cyanide<Scalar> CN;
    typedef Dumux::Citrate<Scalar> Citrate;
    typedef Dumux::Biosub<Scalar> Biosub;
    typedef Dumux::Air<Scalar> Air;
    typedef Dumux::Biofilm<TypeTag, Scalar> Biofilm;
    typedef Dumux::H2O<Scalar> Water;
    typedef Dumux::NullParameterCache ParameterCache;

    typedef BinaryCoeff::H2O_Air H2O_Air;

    /****************************************
     * Fluid phase related static parameters
     ****************************************/

    static const int numPhases = 2; // liquid and gas phases //brine and air
    static const int numSPhases =  4;//  solid phases //TODO: solid Au, dissolving biofilm, precipitating biofilm, immobile nanoAu
    static const int wPhaseIdx = 0; // index of the liquid phase
    static const int nPhaseIdx = 1; // index of the gas phase
    static const int auPhaseIdx = numPhases; // index of solid Au
    static const int nanoAuPhaseIdx = numPhases+1; // index of immobile nanoAu
    static const int dPhaseIdx = numPhases+2; // index of the dissolving biofilm
    static const int pPhaseIdx = numPhases+3; // index of precipitating biofilm


    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx)
    {
        static std::string name[] = {
            "w",
            "n",
            "au",
           "nanoAu",
            "d",
            "p"

        };

        assert(0 <= phaseIdx && phaseIdx < numPhases+numSPhases);
        return name[phaseIdx];
    }

    /*!
     * \brief Return whether a phase is liquid
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        return phaseIdx != nPhaseIdx;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are indepent on the fluid composition. This assumtion is true
     * if Henry's law and Rault's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        return true;
    }

    /****************************************
     * Component related static parameters
     ****************************************/
    static const int numComponents = 7; // brine, air, substrate, cyanide, citrate, Au (I), mobile nano Au

    static const int numSecComponents = 0;

    static const int H2OIdx = 0;
    static const int BrineIdx = 0;
    static const int AirIdx = 1;
    static const int wCompIdx = BrineIdx;
    static const int nCompIdx = AirIdx;

    static const int AuIIdx  = 2; //Au (I)
    static const int nanoAuIdx  = 3;//mobile nano Au
    static const int CNIdx  = 4; //cyanide
    static const int CitrateIdx  = 5; //citrate
    static const int BiosubIdx  = 6; //substrate/nutrient

    static const int AuSolidIdx  = numComponents; // solid Au
    static const int immNanoAuIdx  = numComponents + 1; //immobile nano Au
    static const int dissBiofilmIdx  = numComponents + 2; //dissolving biofilm
    static const int precBiofilmIdx  = numComponents + 3;//precipitating biofilm


    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {

        switch (compIdx) {
        case H2OIdx: return Water::name();
//         case BrineIdx: return Brine::name();
    case AirIdx: return Air::name();
    case AuIIdx: return "AuComplex";//
    case nanoAuIdx: return "nanoAu"; //mobile nano Au
    case CNIdx: return "cyanide";
    case CitrateIdx: return "citrate";
    case BiosubIdx: return "substrate";

        break;
        default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx); break;
        };
    }

    /*!
     * \brief Return the molar mass of a component in [kg/mol].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
       Scalar M = 0;
        switch (compIdx) {
        case H2OIdx: M = H2O::molarMass();break;
        // actually, the molar mass of brine is only needed for diffusion
        // but since chloride and sodium are accounted for seperately
        // only the molar mass of water is returned.
        case AirIdx: M = Air::molarMass();break;
        case AuIIdx: M = Au::molarMass();break;
        case nanoAuIdx: M = Au::molarMass();break;
        case CNIdx: M = CN::molarMass();break;
        case CitrateIdx: M = Citrate::molarMass();break;
        case BiosubIdx: M = Biosub::molarMass();break;
        default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);break;
        };
        return M;
    }

    static Scalar precipitateMolarMass(int phaseIdx)
    {
        if(phaseIdx==auPhaseIdx||phaseIdx==nanoAuPhaseIdx)
            return Au::molarMass();

        else if(phaseIdx==dPhaseIdx||phaseIdx==pPhaseIdx)
           return Biofilm::molarMass();

        else
        DUNE_THROW(Dune::InvalidStateException, "Invalid precipitate phase index " << phaseIdx);
        return 1;
    }

    static Scalar precipitateDensity(int phaseIdx) //phaseIdx = 0 means water!
    {
        if(phaseIdx==auPhaseIdx||phaseIdx==nanoAuPhaseIdx)
            return Au::density();

        else if(phaseIdx==dPhaseIdx||phaseIdx==pPhaseIdx)
           return Biofilm::density();

        else
        DUNE_THROW(Dune::InvalidStateException, "Invalid precipitate phase index " << phaseIdx);
        return 1;
    }

    static Scalar precipitateMolarDensity(int phaseIdx)
    {
        return precipitateDensity(phaseIdx)/precipitateMolarMass(phaseIdx);
    }

    /****************************************
     * thermodynamic relations
     ****************************************/

    static void init()
    {
//        init(/*startTemp=*/273.15, /*endTemp=*/623.15, /*tempSteps=*/100,
//             /*startPressure=*/-10, /*endPressure=*/40e6, /*pressureSteps=*/200);
        init(/*startTemp=*/295.15, /*endTemp=*/305.15, /*tempSteps=*/10,
             /*startPressure=*/1e4, /*endPressure=*/40e6, /*pressureSteps=*/200);

    }

    static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
                     Scalar startPressure, Scalar endPressure, int pressureSteps)
    {
        std::cout << "Initializing tables for the pure-water properties.\n";
//        H2O_Tabulated::init(startTemp, endTemp, tempSteps,
        Water::init(startTemp, endTemp, tempSteps,
                            startPressure, endPressure, pressureSteps);
     }

     /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure brine.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     * Equation given in:    - Batzle & Wang (1992)
     *                         - cited by: Bachu & Adams (2002)
     *                           "Equations of State for basin geofluids"
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          const ParameterCache &paramCache,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases + numSPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        switch (phaseIdx) {
            case wPhaseIdx:
                return waterDensity_(temperature,
                                      pressure);
        case nPhaseIdx:
                return airDensity_(temperature,
                                   pressure);
        case dPhaseIdx:  // BiofilmDissolving
                return Biofilm::density();

        case pPhaseIdx://BiofilmPrecipitating
                return Biofilm::density();

        case auPhaseIdx:
                return Au::density();

        case nanoAuPhaseIdx:
                return Au::density();

            default:
                DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx); break;
            }
      }
    static Scalar waterDensity(const Scalar Temp,
                                const Scalar p)
                                 {
        return waterDensity_(Temp, p);  //(Temp, p, xlTC, xlH2O, xlSal);
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure brine.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     * Equation given in:    - Batzle & Wang (1992)
     *                         - cited by: Bachu & Adams (2002)
     *                           "Equations of State for basin geofluids"
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            const ParameterCache &paramCache,
                            int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == wPhaseIdx)
        {
            // assume pure brine for the liquid phase. TODO: viscosity
            // of mixture
//             Scalar XSal = fluidState.massFraction(lPhaseIdx, NaIdx)         //Salinity= XNa+XCl+XCa
//                             + fluidState.massFraction(lPhaseIdx, ClIdx)
//                             + fluidState.massFraction(lPhaseIdx, CaIdx);
            Scalar result = Water::liquidViscosity(temperature, pressure);  // (temperature, pressure, XSal);
            Valgrind::CheckDefined(result);
            return result;
        }

       else if(phaseIdx == nPhaseIdx)
        {
          Scalar result = Air::gasViscosity(temperature, pressure);
          Valgrind::CheckDefined(result);

            return result;
        }
     else
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);

  }


     /*!
     * \brief Returns the fugacity coefficient [Pa] of a component in a
     *        phase.
     *
     * The fugacity coefficient \f$\phi^\kappa_\alpha\f$ of a
     * component \f$\kappa\f$ for a fluid phase \f$\alpha\f$ defines
     * the fugacity \f$f^\kappa_\alpha\f$ by the equation
     *
     * \f[
     f^\kappa_\alpha := \phi^\kappa_\alpha x^\kappa_\alpha p_\alpha\;.
     \f]
     *
     * The fugacity itself is just an other way to express the
     * chemical potential \f$\zeta^\kappa_\alpha\f$ of the component:
     *
     * \f[
     f^\kappa_\alpha := \exp\left\{\frac{\zeta^\kappa_\alpha}{k_B T_\alpha} \right\}
     \f]
     * where \f$k_B\f$ is Boltzmann's constant.
     */

//    using Base::fugacityCoefficient;
     template <class FluidState>

    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      const ParameterCache &paramCache,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIdx && compIdx < numComponents);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);
        assert(T > 0);
        assert(p > 0);

        if (phaseIdx == nPhaseIdx)
            return 1.0;

        else if (phaseIdx == wPhaseIdx)
        {
        if (compIdx == H2OIdx)
            return Brine::vaporPressure(T)/p;
        else if (compIdx == AirIdx)
            return BinaryCoeff::H2O_Air::henry(T)/p;
        else
            return 1/p;
        }
        else
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase [mol^2 * s / (kg*m^3)]
     *
     * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \grad mu_\kappa \f]
     *
     * where \f$\mu_\kappa\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
     * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
     * fugacity \f$f_\kappa\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
     * pressure and temperature.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       const ParameterCache &paramCache,
                                       int phaseIdx,
                                       int compIdx)
    {
        return 1.587e-9;  //[m²/s]    //J. Phys. D: Appl. Phys. 40 (2007) 2769-2776
        // TODO!
        // DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    };


    /*!
     * \brief Thermal conductivity of a fluid phase [W/(m^2 K/m)].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */

     /*!
     * \brief Given the phase composition, return the specific
     *        phase enthalpy [J/kg].
     */
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                           const ParameterCache &paramCache,
                           int phaseIdx)
    {

       DUNE_THROW(Dune::NotImplemented, "enthalpy");


    };

    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      const ParameterCache &paramCache,
                                      int phaseIdx)
    {
           DUNE_THROW(Dune::NotImplemented, "Thermal conductivity");

    }

      /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        [J/kg]
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               const ParameterCache &paramCache,
                               int phaseIdx)
    {
        // TODO!
        DUNE_THROW(Dune::NotImplemented, "Heat capacities");
    }

    /*!
     * \brief Given the phase compositions, return the binary
     *        diffusion coefficent of two components in a phase.
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             const ParameterCache &paramCache,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIIdx && compIIdx < numComponents);
        assert(0 <= compJIdx && compJIdx < numComponents);

       //Scalar temperature = fluidState.temperature(phaseIdx);
       //Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == wPhaseIdx) {
            assert(compIIdx == H2OIdx);
            Scalar result = 0.0;
          // if(compJIdx == nCompIdx)
             //result = 1.e-9;  //TODO: Attention, this is completely arbitrary!!
          // else if (compJIdx <numComponents)
              //  result = 1.e-9;  //[m²/s]

            return result;
        }
        else {
            assert(phaseIdx == nPhaseIdx);
                //DUNE_THROW(Dune::NotImplemented, "Binary difussion coefficient : Incorrect compIdx");
            return 0.0;
        }
    };

private:
   static Scalar airDensity_( Scalar T,   // T = temperature
                              Scalar pn)
    {
        Scalar airDensity = Air::gasDensity(T, pn);
        return airDensity;
    }

    /*!
     * \brief The density of pure brine at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     * Equations given in:    - Batzle & Wang (1992)
     *                        - cited by: Adams & Bachu in Geofluids (2002) 2, 257-271
     */

    static Scalar waterDensity_(Scalar T,
                                 Scalar pw)
    {
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(pw);

//         if(T < 273.15) {
//             DUNE_THROW(NumericalProblem,
//                        "Liquid density for Brine and CO2 is only "
//                        "defined above 273.15K (is" << T << ")");
//         }
//         if(pw >= 2.5e8) {
//             DUNE_THROW(NumericalProblem,
//                        "Liquid density for Brine and CO2 is only "
//                        "defined below 250MPa (is" << pw << ")");
//         }

        Scalar rho_water = Water::liquidDensity(T, pw);
         return rho_water;
    }

public:
};

} // end namespace
} // end namespace

#endif
